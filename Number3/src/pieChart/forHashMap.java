package pieChart;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.chart.PieChart;
import javafx.stage.Stage;
import searchVariousItems.inText.countingElements;
import sorted.forStructure;

import java.io.IOException;
import java.util.HashMap;
import java.util.Scanner;

public class forHashMap extends Application {
    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        countingElements cE = new countingElements();
        forStructure fS = new forStructure();
        HashMap<Character, Integer> forPieChart;
        Scanner scanner = new Scanner(System.in);
        System.out.print("Введите название файла(без расширения): ");
        String nameFile = scanner.nextLine();
        forPieChart = fS.ascending(cE.countingElement(nameFile));
        PieChart.Data data[] = new PieChart.Data[forPieChart.size()];
        int i = 0;
        String keysHM[] = new String[forPieChart.size()];
        for (Character key : forPieChart.keySet()) {
            keysHM[i] = String.valueOf(key);
            i++;
        }
        i = 0;
        int valuesHM[] = new int[forPieChart.size()];
        for (int value : forPieChart.values()) {
            valuesHM[i] = value;
            i++;
        }
        for (i = 0; i < forPieChart.size(); i++) {

            data[i] = new PieChart.Data(keysHM[i], valuesHM[i]);

        }

        PieChart pie = new PieChart(FXCollections.observableArrayList(data));
        Group group = new Group(pie);
        Scene scene = new Scene(group, 500, 400);
        primaryStage.setTitle("Кол-во различных символов в файле");
        primaryStage.setScene(scene);
        primaryStage.show();
    }
}
