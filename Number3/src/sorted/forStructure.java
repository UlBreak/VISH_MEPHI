package sorted;

import searchVariousItems.inText.countingElements;

import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import static java.util.stream.Collectors.*;


public class forStructure {
    public HashMap<Character, Integer> ascending(HashMap<Character, Integer> originalStructure) throws Exception {
        HashMap<Character, Integer> sortedStructure = originalStructure
                .entrySet()
                .stream()
                .sorted(Collections.reverseOrder(Map.Entry.comparingByValue()))
                .collect(
                        toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e2,
                                LinkedHashMap::new));
        return sortedStructure;
    }
}
