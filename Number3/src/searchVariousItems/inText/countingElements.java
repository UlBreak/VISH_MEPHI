package searchVariousItems.inText;

import shiftAndDelete.atCharArray.fromLine;

import java.io.*;
import java.util.HashMap;
import java.util.Locale;

public class countingElements {
    public HashMap<Character, Integer> countingElement(String nameFile) throws IOException {
        HashMap<Character, Integer> symbolsAndCounter = new HashMap<>();
        Character symbol;
        char[] lineInArray;
        fromLine fLine = new fromLine();
        int count;

        File file = new File(nameFile + ".txt");
        FileReader fr = new FileReader(file);
        BufferedReader reader = new BufferedReader(fr);
        String line = reader.readLine();

        while (line != null) {
            line = line.toLowerCase(Locale.ROOT);
            lineInArray = new char[line.length()];
            for (int i = 0; i < line.length(); i++) {
                lineInArray[i] = line.charAt(i);
            }
            while (lineInArray.length > 0) {
                count = 1;
                symbol = lineInArray[0];
                if (symbolsAndCounter.containsKey(symbol))
                    symbolsAndCounter.put(symbol, symbolsAndCounter.get(symbol) + 1);
                else symbolsAndCounter.put(symbol, 1);
                for (int i = 1; i < lineInArray.length; i++) {
                    if (symbol == lineInArray[i]) {
                        symbolsAndCounter.put(symbol, symbolsAndCounter.get(symbol) + 1);
                        count++;
                    }
                }
                while (count != 0) {
                    lineInArray = fLine.shiftAndDelete(lineInArray, symbol);
                    count--;
                }
            }
            line = reader.readLine();
        }
        return symbolsAndCounter;
    }
}
