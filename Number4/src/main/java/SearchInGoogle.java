import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.Scanner;

public class SearchInGoogle {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.setProperty("webdriver.chrome.driver", "/Library/Java/JavaVirtualMachines/chromedriver");
        ChromeDriver driver = new ChromeDriver();
        driver.get("https://www.google.ru/");
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(3));
        System.out.println("Enter your request");
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@class=\"gLFyf gsfi\"]"))).sendKeys(scanner.nextLine(), Keys.ENTER);
    }
}
