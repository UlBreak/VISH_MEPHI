package solution;

public class Solutions {
    private int[] Buffer=new int[8];
    private float MV;
    private void bufferFilling(){
        for (int i=0;i<8;i++){
            this.Buffer[i]= (int) (Math.random()*9);
        }
    }
    private void shiftBuffer(){
        int[] NewBuffer=new int[8];
        for(int i=0;i<7;i++){
            NewBuffer[i]=Buffer[i+1];
        }
        NewBuffer[7]=(int) (Math.random()*9);
        Buffer=NewBuffer;
    }
    private void medianValue(){
        MV=((float) Buffer[3]+(float) Buffer[4])/2;
        System.out.println("["+MV+"]");
    }
    public void fifo(){
        int count=1;
        bufferFilling();
        while (count<=100){
            printOutBuffer();
            shiftBuffer();
            printOutBuffer();
            medianValue();
            count++;
        }
    }
    private void printOutBuffer(){
        System.out.print("[");
        for(int i=0;i<8;i++){
            System.out.print(this.Buffer[i]+",");
        }
        System.out.print("]->");
    }

}
