//Требуется написать программу которая будет решать уравнение ax^2+bx+c==0 при любых a,b,c .
//Решение уравнение нужно вынести в отдельный метод, который ожидает целочисленные a,b,c на вход и печатает строку.

import solution.quadraticEquation.QuadraticEquation;

public class Equation {
    public static void main(String[] args) {
        QuadraticEquation qe = new QuadraticEquation();
        qe.solution();
    }
}
