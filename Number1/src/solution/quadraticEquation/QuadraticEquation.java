package solution.quadraticEquation;

import java.util.Scanner;

public class QuadraticEquation {
    private int A, B, C;
    double D;
    private double X1, X2;

    private void getElements() {
        Scanner scan = new Scanner(System.in);
        System.out.print("Введите переменную A: ");
        this.A = scan.nextInt();
        System.out.print("Введите переменную B: ");
        this.B = scan.nextInt();
        System.out.print("Введите переменную C: ");
        this.C = scan.nextInt();
    }

    private void getDiscriminant(int a, int b, int c) {
        double B = (double) b;
        this.D = (Math.pow(B, 2.0) - 4 * (double) a * (double) c);
        System.out.println("D-" + D);
    }

    private void getSolutionEquation(int b, double d, int a) {
        this.X1 = ((double) -b + Math.sqrt(d)) / (2 * (double) a);
        this.X2 = ((double) -b - Math.sqrt(d)) / (2 * (double) a);
    }

    public void solution() {
        String S = null;
        getElements();
        getDiscriminant(A, B, C);
        if (D > 0.0) {
            getSolutionEquation(B, D, A);
            S = "Уравнение ax^2+bx+c==0" + "\nПри a=" + A + ",b=" + B + ",c=" + C + "\nИмеет два решение:\nX1=" + X1 + "\n" + "X2=" + X2;
        }
        if (D == 0.0) {
            getSolutionEquation(B, D, A);
            S = "Уравнение ax^2+bx+c==0" + "\nПри a=" + A + ",b=" + B + ",c=" + C + "\nИмеет единственное решение:\nX=" + X1;
        }
        if (D < 0.0)
            S = "Уравнение ax^2+bx+c==0" + "\nПри a=" + A + ",b=" + B + ",c=" + C + "\nНе имеет решения";
        System.out.println(S);
    }


}
