package ru.hh.methods;


import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


import java.time.Duration;
import java.util.*;


public class Main {

    public static void main(String[] args) throws InterruptedException {


        Vacancies openListVacancies = new Vacancies();
        SalaryStatistics salaryStatics = new SalaryStatistics();
        GetSalaryFromLinks getSalaryFromLinks = new GetSalaryFromLinks();

        System.setProperty("webdriver.chrome.driver", "/Library/Java/JavaVirtualMachines/chromedriver");
        ChromeDriver driver = new ChromeDriver();
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(3));
        driver = openListVacancies.openPageVacancies(driver);

        ((JavascriptExecutor) driver).executeScript("window.open()");
        ArrayList<String> tabs = new ArrayList<>(driver.getWindowHandles());

        LinkedList<Double> salary = new LinkedList<>();

        driver.switchTo().window(tabs.get(1));
        driver.get("https://www.cbr.ru/search/");
        driver.switchTo().window(tabs.get(0));

        String url = driver.getCurrentUrl();
        getSalaryFromLinks.onlyNumbersFromSalaryInRublesAscendingOrder(driver, salary);
        driver.get(url);


        Boolean isPresent = driver.findElements(By.xpath("//a[@data-qa='pager-next']")).size() > 0;
        while (isPresent) {

//            driver.findElement(By.xpath("//a[@data-qa='pager-next']")).click();
            wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//a[@data-qa='pager-next']"))).click();
            url = driver.getCurrentUrl();
            getSalaryFromLinks.onlyNumbersFromSalaryInRublesAscendingOrder(driver, salary);
            driver.get(url);
            Thread.sleep(7000);
            isPresent = driver.findElements(By.xpath("//a[@data-qa='pager-next']")).size() > 0;
        }
        System.out.println("Average salary: " + salaryStatics.averageSalary(salary) + "руб.");
        System.out.println("Modal salary: " + salaryStatics.modalSalary(salary) + "руб.");
        System.out.println("Median salary: " + salaryStatics.medianSalary(salary) + "руб.");
        driver.quit();
    }

}
