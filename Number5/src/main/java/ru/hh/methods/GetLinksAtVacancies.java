package ru.hh.methods;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.ArrayList;
import java.util.List;

//ready
public class GetLinksAtVacancies {
    protected List<String> GetLinks(ChromeDriver driver){
        List<WebElement> xpathElement = driver.findElements(By.xpath("//a[@data-qa='vacancy-serp__vacancy-title']"));
        List<String> vacanciesLinks = new ArrayList<>();
        for (int i = 0; i < xpathElement.size(); i++){
            vacanciesLinks.add(xpathElement.get(i).getAttribute("href"));
        }
        return vacanciesLinks;
    }
}
