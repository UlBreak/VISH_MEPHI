package ru.hh.methods;

import java.util.*;

public class SalaryStatistics {
    String averageSalary(LinkedList<Double> salary) {
        double size = salary.size();
        double sum = 0;
        for (int i = 0; i < size; i++) {
            sum += salary.get(i);
        }
        String result = String.format("%.2f",sum/size);

        return result;
    }

    Double modalSalary(LinkedList<Double> salary) {
        int size = salary.size();
        double symbol;
        HashMap <Double, Integer> hMSalary = new HashMap<>();
        for(int i = 0;i<size;i++){
            symbol=salary.get(i);
            if(hMSalary.containsKey(symbol)){
                hMSalary.put(symbol,hMSalary.get(symbol)+1);
            }
            else hMSalary.put(symbol,1);
        }
        ArrayList<Integer> amount = new ArrayList<>(hMSalary.values());
        ArrayList<Double> salarys = new ArrayList<>(hMSalary.keySet());
        int index = 0;
        int maxAmount = amount.get(0);
        for (int i=1;i<amount.size();i++){
            if (maxAmount<amount.get(i)){
                maxAmount=amount.get(i);
                index=i;
            }
        }
        return salarys.get(index);
    }

    double medianSalary(LinkedList<Double> salary) {
        Collections.sort(salary);
        int size = salary.size();
        double medianSal;
        if (size % 2 == 1)
            medianSal = salary.get(size / 2 + 1);
        else
            medianSal = (salary.get(size / 2) + salary.get(size / 2 + 1)) / 2.0;
        return medianSal;
    }

}