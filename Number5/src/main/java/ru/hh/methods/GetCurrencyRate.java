package ru.hh.methods;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.ArrayList;

//работает, но реализация в строчках 19-22 говнище

public class GetCurrencyRate {


    protected double currencyRate(String currency,ChromeDriver driver) throws InterruptedException {

        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(3));

        ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
        driver.switchTo().window(tabs.get(1));

        driver.manage().window().maximize();

        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@id='Text']"))).clear();
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@id='Text']"))).sendKeys(currency);




        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[@type='submit']"))).click();
        String rateAtStr = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@id='Text']"))).getAttribute("innerText");
        rateAtStr = rateAtStr.replace("1/", "").replace(",", ".");
        Double rate = Double.parseDouble(rateAtStr);
        String result = String.format("%.2f", rate);
        rate = Double.parseDouble(result.replace(",", "."));
        return rate;
    }
}
