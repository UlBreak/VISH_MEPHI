package ru.hh.methods;

import org.openqa.selenium.By;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.Scanner;

//ready
public class Vacancies {

    public ChromeDriver openPageVacancies(ChromeDriver driver) throws InterruptedException {
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(3));
        driver.manage().window().maximize();
        driver.get("https://hh.ru/");
        String searchTerm = searchTermVacancies();
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@data-qa='search-input']"))).sendKeys(searchTerm);
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[@data-qa='search-button']"))).click();
        return driver;
    }

    protected String searchTermVacancies() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter a search Jobs: ");
        String searchTerm = scanner.nextLine();
        System.out.print("Enter a search Area: ");
        searchTerm += " " + scanner.nextLine();
        return searchTerm;
    }

}
