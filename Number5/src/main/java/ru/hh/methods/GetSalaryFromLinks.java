package ru.hh.methods;


import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class GetSalaryFromLinks {
    protected List<String> getTextOfSalaryFromLinks(ChromeDriver driver) throws InterruptedException {
        GetLinksAtVacancies getLinksAtVacancies = new GetLinksAtVacancies();
        List<String> salary = new ArrayList<>();
        WebElement xpathForSalary;
        List<String> links = getLinksAtVacancies.GetLinks(driver);
        for (int i=0;i<links.size();i++){
            driver.get(links.get(i));
            Thread.sleep(3);
            xpathForSalary = driver.findElement(By.xpath("//div[@data-qa='vacancy-salary']"));

            salary.add(xpathForSalary.getAttribute("outerText"));
        }
        return salary;
    }
    protected List<String> DeleteEmptySalary(List<String> listSalary){
        String textFromList;
        for (int i =0; i<listSalary.size();i++){
            textFromList = listSalary.get(i);
            if (textFromList.equals("з/п не указана")){
                listSalary.remove(i);
                i--;
            }
        }
        return listSalary;
    }
    protected String deleteNotSpecifiedSalary(String textOfSalary){
        String[] test = textOfSalary.split(" ");
        if((test[0].equals("от")&&test[2].equals("до"))) {
            textOfSalary=test[1]+" "+ test[2]+" "+test[3]+" "+test[4];
        }
        else
            textOfSalary=test[1]+" "+test[2];

        return textOfSalary;
    }

    protected List<Double> translateStringInDouble(List<String> salary,ChromeDriver driver) throws InterruptedException {
        LinkedList<Double> newSalaryList = new LinkedList<>();
        Double currency;
        Double firstNumber;
        Double secondNumber;
        String[] splitText;
        GetCurrencyRate getCurrencyRate = new GetCurrencyRate();
        ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
        driver.switchTo().window(tabs.get(1));
        for (int i=0;i<salary.size();i++){
            splitText = salary.get(i).split(" ");
            if(splitText[1].equals("до")){
                firstNumber = Double.parseDouble(splitText[0].replace(" ",""));
                secondNumber = Double.parseDouble(splitText[2].replace(" ",""));
                if (splitText[3].equals("руб.")!=true){
                    currency = getCurrencyRate.currencyRate(splitText[3],driver);
                    newSalaryList.add((firstNumber+secondNumber)/2*currency);
                }
                else
                    newSalaryList.add((firstNumber+secondNumber)/2);
            }
            else {
                firstNumber = Double.parseDouble(splitText[0].replace(" ", ""));
                if (splitText[1].equals("руб.")!=true) {
                    currency = getCurrencyRate.currencyRate(splitText[1], driver);
                    newSalaryList.add(firstNumber * currency);
                }
                else
                    newSalaryList.add(firstNumber);
            }
        }
        driver.switchTo().window(tabs.get(0));
        return newSalaryList;
    }

    void onlyNumbersFromSalaryInRublesAscendingOrder(ChromeDriver driver,LinkedList<Double> newSalary)
            throws InterruptedException {
        List<String> salary;
        GetSalaryFromLinks getSalaryFromLinks = new GetSalaryFromLinks();
        salary = getSalaryFromLinks.getTextOfSalaryFromLinks(driver);
        salary = getSalaryFromLinks.DeleteEmptySalary(salary);
        for (int i=0;i<salary.size();i++){
            salary.set(i,getSalaryFromLinks.deleteNotSpecifiedSalary(salary.get(i)));
        }
        newSalary.addAll(getSalaryFromLinks.translateStringInDouble(salary,driver));
    }
}


